
import React, { Component } from 'react';
import {View, Text, StyleSheet, Image, TextInput, ScrollView, Button, TouchableOpacity, Alert} from 'react-native';
import Loginimages from '../../../src/assets/Images/image-login.png';


export default class Index extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.columnLogin1}>
                    <Image style={styles.LoginImg} source={Loginimages}/>
                </View>
                <View style={styles.columnLogin2}>
                    <Text style={styles.textLogin2}>My Pets</Text>
                    <Text style={styles.textLogin3}>
                        Taking care of a pet is my Favourite. It helps me to 
                        gainer stress and fatique
                    </Text>
                </View>
                <View style={styles.columnLogin3}>
                    <TextInput style={styles.inputUserPass}/>
                    <Text style={styles.userPass}>USERNAME</Text>
                    <TextInput style={styles.inputUserPass}/>
                    <Text style={styles.userPass}>PASSWORD</Text>
                    <TouchableOpacity style={styles.touchOp}>
                    <Button
                    onPress={() => Alert.alert('Login Button pressed')}
                    title="LOGIN"
                    color="#A57AA5"
                    style={styles.buttonOp}
                    />
                </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        flex: 1
    },
    LoginImg:{
        width: 350,
        height: 250,
        alignItems: 'flex-start'
    },
    columnLogin1: {
        flexDirection: 'column',
        alignItems: 'center',
        flex: 2,
        paddingTop: 75
    },
    columnLogin2: {
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1,
    },
    columnLogin3: {
        flexDirection: 'column',
        alignItems: 'center',
        flex: 2,
        paddingTop: 30
    },
    textLogin2: {
        fontWeight: '600',
        fontSize: 25,
        color: '#ECA1AB'
    },
    textLogin3: {
        color: '#ECA1AB',
        fontSize: 14,
        textAlign: 'center',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop:5
    },
    inputUserPass: {
        borderRadius: 50,
        backgroundColor:'#ECA1AB',
        height: 40,
        width: 300,
        marginTop: 10,
        paddingLeft: 30
    },
    userPass:{
        color: '#ECA1AB',
        fontWeight: 'bold',
        paddingTop: 5
    },
    touchOp: {
        paddingTop: 30,
        // paddingLeft:85,
        // paddingRight:85,
    },
    buttonOp: { 
       width: 500,
       height: 100,
       fontWeight: 'bold'
    }
})

