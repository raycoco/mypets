import React, { Component } from 'react';
import {ScrollView, View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import ProfileIcon from '../../assets/Icons/profile-icon.png';
import LocIcon from '../../assets/Icons/location_on.png';
import LoncIcon from '../../assets/Icons/Lonceng.png';
import ArrowDownicon from '../../assets/Icons/keyboard_arrow_down.png';
import { FlatList } from 'react-native-gesture-handler';

export default class index extends Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.topProfile}>
                    <View style={styles.topProf}>
                        <TouchableOpacity>
                            <Image source={ProfileIcon} style={styles.ImgProfIcon}/>
                        </TouchableOpacity>
                        <Image source={LocIcon} style={styles.ImgLocIcon} />
                        <TouchableOpacity style={styles.cityChoice}>
                            <Text>Denpasar</Text>
                            <Image source={ArrowDownicon} style={styles.ImgLocIcon} />
                        </TouchableOpacity>
                    </View>  
                    <TouchableOpacity>
                        <Image source={LoncIcon} style={styles.LoncengIcon}/>
                    </TouchableOpacity>      
                </View>
                <View>
                    <FlatList>
                        <TouchableOpacity>
                            <Text>
                                Dogs
                            </Text>
                        </TouchableOpacity>
                    </FlatList>
                </View> 
            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor:'white',
        flex:1,
    },
    topProfile :{
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
        paddingTop:50,
    },
    topProf:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 100
    },
    ImgProfIcon:{
        height:75,
        width:75
    },
    ImgLocIcon:{
        height:25,
        width:25
    },
    ArrowDownicon:{

    },
    cityChoice: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})