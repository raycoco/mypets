import { StatusBar } from 'expo-status-bar';
import React,{useEffect, useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './src/pages/Login';
import Listing from './src/pages/Listing';
// import {NavigationContainer} from '@react-navigation/native';
// import {createStackNavigator} from '@react-navigation/stack';

// const MovingNav = createStackNavigator();

// export default () => (
//     <NavigationContainer>
//         <MovingNav.Navigator>
//             <MovingNav.Screen name="Login" component={Login} />
//         </MovingNav.Navigator>
//     </NavigationContainer>
// ); 

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" /> */}
      {/* <Login /> */}
      <Listing />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
